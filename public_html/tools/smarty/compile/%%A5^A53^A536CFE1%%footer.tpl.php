<?php /* Smarty version 2.6.20, created on 2018-06-17 09:13:16
         compiled from E:%5COSPanel%5Cdomains%5Cdiboranew%5Cpublic_html/themes/Earth/footer.tpl */ ?>
	<?php if (! $this->_tpl_vars['content_only']): ?>
			</div>

<!-- Right -->
			<div id="right_column" class="column">
				<?php echo $this->_tpl_vars['HOOK_RIGHT_COLUMN']; ?>

			</div>



			<div id="footer" align="center">
			 <?php echo $this->_tpl_vars['HOOK_FOOTER']; ?>

			</div>
<!-- Footer -->
<div id="footer-content">
	<div class="column1" style="width: 510px">
		
		<p style="font-size: 11px;">Добро пожаловать на сайт Диборы! Наша компания – это семейное предприятие, которое объединяет 3 поколения людей, любящих свое дело. Мы разднляем семейные ценности, верим в доброту и порядочность и  в то, что растения выращенные с любовью всегда крепче и здоровее. 
		Мы занимаемся как однолетними, так и многолетними растениями украинского, голландского, японского и польского происхождения. Будем рады, если представленный каталог растений Вы найдете для себя интересным. Все растения прошли сертификацию и пригодны для условий местного климата. Среди них есть немало уникальных и занесенных в Красные Книги многих стран мира. Мы старались подобрать такой ассортимент, чтобы Ваш сад получился неповторимым и радовал Вас каждый день!</p>

	</div>
	<div class="column2" style="width: 333px">
		<b>Контакты</b><br/>
		<table>
		<tr>
			<td width="150px" valign="top">
			Питомник:<br/>
			ул.Ленина, 22<br/>
			с. Сулимовка<br/>
			Бориспольский район,<br/> 
			Киевская область<br/>
			+38 (067) 464-2607 (Консультации по уходу за растениями)<br/> 
			</td>
			<td style="padding-left: 15px;" valign="top">
			Офис:<br/> 
			+38 (067) 464-2607<br/> 
			+38 (093) 621-8115<br/> 
			Е: <a href="mailto:sales@dibora.com.ua">sales@dibora.com.ua</a><br/>
			<a href="<?php echo $this->_tpl_vars['base_dir']; ?>
sitemap.php">Карта сайта</a><br/>
			&copy;Все права защищены
			</td>
		</tr>
		</table>
	</div>
</div>

		</div>
	<?php endif; ?>
    
	</body>
</html>