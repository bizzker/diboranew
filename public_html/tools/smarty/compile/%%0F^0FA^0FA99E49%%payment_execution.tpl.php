<?php /* Smarty version 2.6.20, created on 2014-01-13 20:39:05
         compiled from /home/devteamc/domains/dibora.com.ua/public_html/modules/bankwire/payment_execution.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'l', '/home/devteamc/domains/dibora.com.ua/public_html/modules/bankwire/payment_execution.tpl', 1, false),array('function', 'displayPrice', '/home/devteamc/domains/dibora.com.ua/public_html/modules/bankwire/payment_execution.tpl', 24, false),)), $this); ?>
<?php ob_start(); ?><?php echo smartyTranslate(array('s' => 'Bank wire payment','mod' => 'bankwire'), $this);?>
<?php $this->_smarty_vars['capture']['path'] = ob_get_contents(); ob_end_clean(); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['tpl_dir'])."./breadcrumb.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<h2><?php echo smartyTranslate(array('s' => 'Order summary','mod' => 'bankwire'), $this);?>
</h2>

<?php $this->assign('current_step', 'payment'); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['tpl_dir'])."./order-steps.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['nbProducts'] <= 0): ?>
	<p class="warning"><?php echo smartyTranslate(array('s' => 'Your shopping cart is empty.'), $this);?>
</p>
<?php else: ?>

<h3><?php echo smartyTranslate(array('s' => 'Bank wire payment','mod' => 'bankwire'), $this);?>
</h3>
<form action="<?php echo $this->_tpl_vars['this_path_ssl']; ?>
validation.php" method="post">
<p>
	<img src="<?php echo $this->_tpl_vars['this_path']; ?>
privat24.jpg" alt="<?php echo smartyTranslate(array('s' => 'bank wire','mod' => 'bankwire'), $this);?>
" style="float:left; margin: 0 10px 5px 0;" />
	<?php echo smartyTranslate(array('s' => 'You have chosen to pay by bank wire.','mod' => 'bankwire'), $this);?>

	<br/><br />
	<?php echo smartyTranslate(array('s' => 'Here is a short summary of your order:','mod' => 'bankwire'), $this);?>

</p>
<table style="width: 500px; border: 1px solid #eee;">
    <tr>
        <td style="padding: 2px;"><?php echo smartyTranslate(array('s' => 'The total amount of your order is','mod' => 'bankwire'), $this);?>
:</td>
        <td style="padding: 2px;"><span id="amount" class="price"><?php echo Tools::displayPriceSmarty(array('price' => $this->_tpl_vars['total']), $this);?>
</span></td>
    </tr>
    <tr>
        <td style="padding: 2px;">Реквизиты карты:</td>
        <td style="padding: 2px;"><?php echo $this->_tpl_vars['bankwireDetails']; ?>
</td>
    </tr>
    <tr>
        <td style="padding: 2px;">Имя держателя карты:</td>
        <td style="padding: 2px;"><?php echo $this->_tpl_vars['bankwireOwner']; ?>
</td>
    </tr>
</table>
<p>
	<br /><br />
	<b><?php echo smartyTranslate(array('s' => 'Please confirm your order by clicking \'I confirm my order\'','mod' => 'bankwire'), $this);?>
.</b>
</p>
<p class="cart_navigation">
	<a href="<?php echo $this->_tpl_vars['base_dir_ssl']; ?>
order.php?step=3" class="button_large"><?php echo smartyTranslate(array('s' => 'Other payment methods','mod' => 'bankwire'), $this);?>
</a>
	<input type="submit" name="submit" value="<?php echo smartyTranslate(array('s' => 'I confirm my order','mod' => 'bankwire'), $this);?>
" class="exclusive_large" />
</p>
</form>
<?php endif; ?>