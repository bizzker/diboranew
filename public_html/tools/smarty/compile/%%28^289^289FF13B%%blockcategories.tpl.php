<?php /* Smarty version 2.6.20, created on 2014-01-13 10:52:11
         compiled from /home/devteamc/domains/dibora.com.ua/public_html/modules/blockcategories/blockcategories.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'l', '/home/devteamc/domains/dibora.com.ua/public_html/modules/blockcategories/blockcategories.tpl', 5, false),)), $this); ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['js_dir']; ?>
tools/treeManagement.js"></script>

<!-- Block categories module -->
<div id="categories_block_left" class="block">
	<h4><?php echo smartyTranslate(array('s' => 'Categories','mod' => 'blockcategories'), $this);?>
</h4>
	<div class="block_content">
		<ul class="tree <?php if ($this->_tpl_vars['isDhtml']): ?>dhtml<?php endif; ?>">
		<?php echo $this->_tpl_vars['htmlOutput']; ?>

		</ul>
	</div>
</div>
<script type="text/javascript">
// <![CDATA[
	// we hide the tree only if JavaScript is activated
	$('div#categories_block_left ul.dhtml').hide();
// ]]>
</script>
<!-- /Block categories module -->