<?php /* Smarty version 2.6.20, created on 2014-01-13 20:39:23
         compiled from /home/devteamc/domains/dibora.com.ua/public_html/modules/bankwire/payment_return.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'l', '/home/devteamc/domains/dibora.com.ua/public_html/modules/bankwire/payment_return.tpl', 2, false),)), $this); ?>
<?php if ($this->_tpl_vars['status'] == 'ok'): ?>
	<p><?php echo smartyTranslate(array('s' => 'Your order on','mod' => 'bankwire'), $this);?>
 <?php echo smartyTranslate(array('s' => 'is complete.','mod' => 'bankwire'), $this);?>

		<br /><br />
    <p class="bold">Детали оплаты заказа:</p>
    <br />
    <table style="width: 500px; border: 1px solid #eee; margin: 0 auto;">
        <tr>
            <td style="padding: 2px;"><?php echo smartyTranslate(array('s' => 'an amout of','mod' => 'bankwire'), $this);?>
:</td>
            <td style="padding: 2px;"><span id="amount" class="price"><?php echo $this->_tpl_vars['total_to_pay']; ?>
</span></td>
        </tr>
        <tr>
            <td style="padding: 2px;">Реквизиты карты:</td>
            <td style="padding: 2px;"><?php echo $this->_tpl_vars['bankwireDetails']; ?>
</td>
        </tr>
        <tr>
            <td style="padding: 2px;">Имя держателя карты:</td>
            <td style="padding: 2px;"><?php echo $this->_tpl_vars['bankwireOwner']; ?>
</td>
        </tr>
        <tr>
            <td colspan="2" style="color: #cc706c; text-align: center; padding: 5px;">ВАЖНО! Пожалуйста, уточняйте наличие всех растений перед оплатой.</td>
        </tr>
    </table>


		<br /><br /><?php echo smartyTranslate(array('s' => 'An e-mail has been sent to you with this information.','mod' => 'bankwire'), $this);?>

		<br /><br /><span class="bold"><?php echo smartyTranslate(array('s' => 'Your order will be sent as soon as we receive your settlement.','mod' => 'bankwire'), $this);?>
</span>
		<br /><br /><?php echo smartyTranslate(array('s' => 'For any questions or for further information, please contact our','mod' => 'bankwire'), $this);?>
 <a href="<?php echo $this->_tpl_vars['base_dir_ssl']; ?>
contact-form.php"><?php echo smartyTranslate(array('s' => 'customer support','mod' => 'bankwire'), $this);?>
</a>.
	</p>
<?php else: ?>
	<p class="warning">
		<?php echo smartyTranslate(array('s' => 'We noticed a problem with your order. If you think this is an error, you can contact our','mod' => 'bankwire'), $this);?>
 
		<a href="<?php echo $this->_tpl_vars['base_dir_ssl']; ?>
contact-form.php"><?php echo smartyTranslate(array('s' => 'customer support','mod' => 'bankwire'), $this);?>
</a>.
	</p>
<?php endif; ?>