<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи, язык WordPress и ABSPATH. Дополнительную информацию можно найти
 * на странице {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется сценарием создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'devteamc_blogdib');

/** Имя пользователя MySQL */
define('DB_USER', 'devteamc_blogdib');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'pzon1gQ6');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется снова авторизоваться.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'o3Wf<1-T$M;_c]Pj3=-lz|VH:K:gr*J.HtzF<hm$8cG/&+mZ/ST]Sby).t+NM+)L');
define('SECURE_AUTH_KEY',  'nUw8mCgS. &6_}GloHFNc:rx=~?D> i<DQ/Laxn8{*rgy WNg+:x$cv5^wSiKkOs');
define('LOGGED_IN_KEY',    '(^os_Gy4rt|DF#Qb9nLT!Ln/~&:k7W/f_hl_<+|<n9gH+#`P-4uUO}T85vZUadsM');
define('NONCE_KEY',        ';y;k%H2+Lz1!l[HX5i/-0|!OBFl*j}Eh|L^tS71I@1C+%ix+}C1pgO$`sW#?2|x7');
define('AUTH_SALT',        '|.;F79Cr6_QrK7 ;+Crn&bi8WY*#&7.Gl|sDmLQ)A1u80 uNQ;uZztWlEO3?^Tbt');
define('SECURE_AUTH_SALT', '@/Kq?+R/caEd}xu{;ls0#JZwIsc<Ts#P 5c&Z#2eGUXRpN{b,3v!t)^iqG(cMN+o');
define('LOGGED_IN_SALT',   '+;sh,vp$c2*<mcn=sc|F? Nmr1$CAix[Oi;eI^DY5=6k+#YHLtW0:-$0_bB_z]bX');
define('NONCE_SALT',       '1$}L-gZjhOvRIz@0prd_=yFu}0V0r2E|hJ3oE$,<]KA/xe^)-MX/e~|HL|jQrSW<');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько блогов в одну базу данных, если вы будете использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Язык локализации WordPress, по умолчанию английский.
 *
 * Измените этот параметр, чтобы настроить локализацию. Соответствующий MO-файл
 * для выбранного языка должен быть установлен в wp-content/languages. Например,
 * чтобы включить поддержку русского языка, скопируйте ru_RU.mo в wp-content/languages
 * и присвойте WPLANG значение 'ru_RU'.
 */
define('WPLANG', 'ru_RU');

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Настоятельно рекомендуется, чтобы разработчики плагинов и тем использовали WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
