{if $status == 'ok'}
	<p>{l s='Your order on' mod='bankwire'} {l s='is complete.' mod='bankwire'}
		<br /><br />
    <p class="bold">Детали оплаты заказа:</p>
    <br />
    <table style="width: 500px; border: 1px solid #eee; margin: 0 auto;">
        <tr>
            <td style="padding: 2px;">{l s='an amout of' mod='bankwire'}:</td>
            <td style="padding: 2px;"><span id="amount" class="price">{$total_to_pay}</span></td>
        </tr>
        <tr>
            <td style="padding: 2px;">Реквизиты карты:</td>
            <td style="padding: 2px;">{$bankwireDetails}</td>
        </tr>
        <tr>
            <td style="padding: 2px;">Имя держателя карты:</td>
            <td style="padding: 2px;">{$bankwireOwner}</td>
        </tr>
        <tr>
            <td colspan="2" style="color: #cc706c; text-align: center; padding: 5px;">ВАЖНО! Пожалуйста, уточняйте наличие всех растений перед оплатой.</td>
        </tr>
    </table>


		<br /><br />{l s='An e-mail has been sent to you with this information.' mod='bankwire'}
		<br /><br /><span class="bold">{l s='Your order will be sent as soon as we receive your settlement.' mod='bankwire'}</span>
		<br /><br />{l s='For any questions or for further information, please contact our' mod='bankwire'} <a href="{$base_dir_ssl}contact-form.php">{l s='customer support' mod='bankwire'}</a>.
	</p>
{else}
	<p class="warning">
		{l s='We noticed a problem with your order. If you think this is an error, you can contact our' mod='bankwire'} 
		<a href="{$base_dir_ssl}contact-form.php">{l s='customer support' mod='bankwire'}</a>.
	</p>
{/if}
