{capture name=path}{l s='Bank wire payment' mod='bankwire'}{/capture}
{include file=$tpl_dir./breadcrumb.tpl}

<h2>{l s='Order summary' mod='bankwire'}</h2>

{assign var='current_step' value='payment'}
{include file=$tpl_dir./order-steps.tpl}

{if $nbProducts <= 0}
	<p class="warning">{l s='Your shopping cart is empty.'}</p>
{else}

<h3>{l s='Bank wire payment' mod='bankwire'}</h3>
<form action="{$this_path_ssl}validation.php" method="post">
<p>
	<img src="{$this_path}privat24.jpg" alt="{l s='bank wire' mod='bankwire'}" style="float:left; margin: 0 10px 5px 0;" />
	{l s='You have chosen to pay by bank wire.' mod='bankwire'}
	<br/><br />
	{l s='Here is a short summary of your order:' mod='bankwire'}
</p>
<table style="width: 500px; border: 1px solid #eee;">
    <tr>
        <td style="padding: 2px;">{l s='The total amount of your order is' mod='bankwire'}:</td>
        <td style="padding: 2px;"><span id="amount" class="price">{displayPrice price=$total}</span></td>
    </tr>
    <tr>
        <td style="padding: 2px;">Реквизиты карты:</td>
        <td style="padding: 2px;">{$bankwireDetails}</td>
    </tr>
    <tr>
        <td style="padding: 2px;">Имя держателя карты:</td>
        <td style="padding: 2px;">{$bankwireOwner}</td>
    </tr>
</table>
<p>
	<br /><br />
	<b>{l s='Please confirm your order by clicking \'I confirm my order\'' mod='bankwire'}.</b>
</p>
<p class="cart_navigation">
	<a href="{$base_dir_ssl}order.php?step=3" class="button_large">{l s='Other payment methods' mod='bankwire'}</a>
	<input type="submit" name="submit" value="{l s='I confirm my order' mod='bankwire'}" class="exclusive_large" />
</p>
</form>
{/if}
