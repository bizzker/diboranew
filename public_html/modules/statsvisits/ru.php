<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsvisits}prestashop>statsvisits_504c16c26a96283f91fb46a69b7c8153'] = 'Визиты и посетители';
$_MODULE['<{statsvisits}prestashop>statsvisits_a985996212e180300abc7671c0f87b9a'] = 'Отображает статистику по визитам и посетителям';
$_MODULE['<{statsvisits}prestashop>statsvisits_3b47e772bb8fbd24fba66604d181210d'] = 'Визит это пришедший из интернета в магазин пользователь. До завершения его сессии - это один визит.';
$_MODULE['<{statsvisits}prestashop>statsvisits_f0f438eaff96f1b7297393df4f39a754'] = 'Посетитель - это незнакомец - незарегистрированный, просто просматривающий магазин. Посетитель может много раз посещать ваш магазин.';
$_MODULE['<{statsvisits}prestashop>statsvisits_54067074d24489ddb5654bf46642cb85'] = 'Всего визитов:';
$_MODULE['<{statsvisits}prestashop>statsvisits_23e640d55e56db79971918936e95bf9d'] = 'Всего посетителей:';
$_MODULE['<{statsvisits}prestashop>statsvisits_6602bbeb2956c035fb4cb5e844a4861b'] = 'Руководство';
$_MODULE['<{statsvisits}prestashop>statsvisits_ffbee337f031c2282b311bac40bc8bb9'] = 'Определение интереса посетителя';
$_MODULE['<{statsvisits}prestashop>statsvisits_9d528c1740b4ec88b125b4d10d07bf30'] = 'График посетителей похож на график визитов но обеспечивает дополнительную информацию: Вы хотите что бы посетители возвращались?';
$_MODULE['<{statsvisits}prestashop>statsvisits_deaaec5a2a384a7a1a7c10e355aaaa77'] = 'если это так, поздравляем, ваш сайт хорошо продуман и неоспоримо побеждает.';
$_MODULE['<{statsvisits}prestashop>statsvisits_659a9716f03e76005b7bf2d4eeeddeda'] = 'В противном случае, вывод не такой прост. Проблема может быть эстетическим или эргономическими, или не достаточно предложений. Также возможно, что эти посетители ошибочно пришли на ваш сайт, без конкретного интереса к вашему магазину; этот феномен часто случается с поисковыми роботами.';
$_MODULE['<{statsvisits}prestashop>statsvisits_c49393dfe6a439b15b8fb51993a7e902'] = 'Эта информация по большей части качественная: Вы должны determin интерес отрывочного визита.';
$_MODULE['<{statsvisits}prestashop>statsvisits_39b960b0a5e2ebaaa638d946f1892050'] = 'Количество визитов и уникальных посетителей';
$_MODULE['<{statsvisits}prestashop>statsvisits_d7e637a6e9ff116de2fa89551240a94d'] = 'Просмотры';
$_MODULE['<{statsvisits}prestashop>statsvisits_ae5d01b6efa819cc7a7c05a8c57fcc2c'] = 'Посетители';
