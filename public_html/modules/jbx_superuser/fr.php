<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{jbx_superuser}prestashop>jbx_superuser_74d3c3dba03efd0dba12892e3f0d7b74'] = 'Super Utilisateur';
$_MODULE['<{jbx_superuser}prestashop>jbx_superuser_864b9a543be174c5d14232ff9acabadf'] = 'Vous êtes LE super utilisateur !';
$_MODULE['<{jbx_superuser}prestashop>jbx_superuser_e6d0e1c8fc6a4fcf47869df87e04cd88'] = 'Clients';
$_MODULE['<{jbx_superuser}prestashop>jbx_superuser_09a413a9de81c74a86abbca89c4069ed'] = 'Vous êtes';
$_MODULE['<{jbx_superuser}prestashop>jbx_superuser_61b027a7d66182184afea648f162a2ca'] = 'Aller dans ma boutique !';
$_MODULE['<{jbx_superuser}prestashop>jbx_superuser_981bd418e179236ccd0a522d8ad6bbaa'] = 'Liste des clients';
$_MODULE['<{jbx_superuser}prestashop>jbx_superuser_99dea78007133396a7b8ed70578ac6ae'] = 'Connexion';
$_MODULE['<{jbx_superuser}prestashop>jbx_superuser_8f7f4c1ce7a4f933663d10543562b096'] = 'À propos';
$_MODULE['<{jbx_superuser}prestashop>jbx_superuser_15bbb9d0bbf25e8d2978de1168c749dc'] = 'Site web';
$_MODULE['<{jbx_superuser}prestashop>jbx_superuser_b1b3792328d658c2d30928bd2ed4a172'] = 'Se connecter en tant que ';
?>