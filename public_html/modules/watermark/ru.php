<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{watermark}prestashop>watermark_ee20bb60493f049175fc10c35acd2272'] = 'Водяной знак';
$_MODULE['<{watermark}prestashop>watermark_929bfd56c7ec447627de0591f78d2008'] = 'Защитить изображения водяным знаком';
$_MODULE['<{watermark}prestashop>watermark_fa214007826415a21a8456e3e09f999d'] = 'Хотите удалить?';
$_MODULE['<{watermark}prestashop>watermark_c2c4e6b62c81ed56359e7d990324d1a1'] = 'Для того, чтобы этот модуль правильно работал загрузите картинку с водяным знаком';
$_MODULE['<{watermark}prestashop>watermark_5bece498c51bbfc90b1f4c11120590e6'] = 'Необходима прозрачность.';
$_MODULE['<{watermark}prestashop>watermark_78d0b1c450a31c9728488a6b0a893e8a'] = 'Недостаточный уравень прозрачности.';
$_MODULE['<{watermark}prestashop>watermark_d31c2d99614d8e16430d2d8c99c1f2b0'] = 'Необходимо выравнивание по оси - Y.';
$_MODULE['<{watermark}prestashop>watermark_8fe9c39f4bf87082caabcf3650970c71'] = 'Выравнивание по оси Y вне разрешенного диапазона.';
$_MODULE['<{watermark}prestashop>watermark_c2cf8e33c907a4cc689881dc8fa571c2'] = 'Необходимо выравнивание по оси - X.';
$_MODULE['<{watermark}prestashop>watermark_3a1f788dbe8957be92048606cf0d3fcb'] = 'Выравнивание по оси X вне разрешенного диапазона.';
$_MODULE['<{watermark}prestashop>watermark_a9cac4be0fa0b815376b96f49e1435d7'] = 'Необходим хотя бы один тип картинки';
$_MODULE['<{watermark}prestashop>watermark_3deee3a74625e5d28cdeba65f9c449dc'] = 'картинка должны быть в формате GIF';
$_MODULE['<{watermark}prestashop>watermark_444bcb3a3fcf8389296c49467f27e1d6'] = 'ok';
$_MODULE['<{watermark}prestashop>watermark_c888438d14855d7d96a2724ee9c306bd'] = 'Настройки обновлены';
$_MODULE['<{watermark}prestashop>watermark_c0275788cf97660b52ab23f1f8c7c8d7'] = 'Детали водяного знака';
$_MODULE['<{watermark}prestashop>watermark_b203c3456625a1ae950961bb1da18ef7'] = 'После настройки модуля вам придется перегенерировать картинки используя инструмент в Настройки > Изображения. Водяной знак будет добавлен на все картинки автоматически.';
$_MODULE['<{watermark}prestashop>watermark_f237761e479d501b4ec1ebe0ef8947cb'] = 'Водяной знак пока не загружен';
$_MODULE['<{watermark}prestashop>watermark_c9519bff5a6fe31e0f26b1c52dad7ad8'] = 'Файл водяного знака';
$_MODULE['<{watermark}prestashop>watermark_6ceef3b44f3ca247b2d32b5e934a4d9d'] = 'Должен быть формат GIF';
$_MODULE['<{watermark}prestashop>watermark_3ccc25c1409aec574beb82bad14b5ebc'] = 'Прозрачность водяного знака (0-100)';
$_MODULE['<{watermark}prestashop>watermark_acf0fa79a73731e7d70cb208a62c249a'] = 'Выравнивание водяного знака по X';
$_MODULE['<{watermark}prestashop>watermark_489f2e730102b59aec48b5c27d2cbe1c'] = 'Выравнивание водяного знака по Y';
$_MODULE['<{watermark}prestashop>watermark_bd75d3144c69854488e26ae437bfc2f8'] = 'Выберите типы картинок для защиты водяным знаком';
$_MODULE['<{watermark}prestashop>watermark_b17f3f4dcf653a5776792498a9b44d6a'] = 'Обновить настройки';
$_MODULE['<{watermark}prestashop>watermark_a472c2655d993574100fc8e2c940fc05'] = 'Изображение водяного знака не в GIF формате, пожалуйста, конвертируйте или переименуйте этот';
