<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statssales}prestashop>statssales_45c4b3e103155326596d6ccd2fea0f25'] = 'Продажи и заказы';
$_MODULE['<{statssales}prestashop>statssales_09a6e97d1ccb13182c8fd316450dbebf'] = 'Отображает статусы заказов и продаж';
$_MODULE['<{statssales}prestashop>statssales_b1c94ca2fbc3e78fc30069c8d0f01680'] = 'Все';
$_MODULE['<{statssales}prestashop>statssales_d7778d0c64b6ba21494c97f77a66885a'] = 'Фильтр';
$_MODULE['<{statssales}prestashop>statssales_0d6f98eca901ce80817bfe6a475fa45c'] = 'Всего размещенных заказов:';
$_MODULE['<{statssales}prestashop>statssales_4d2aaca86fca7a268ce7b6a512d68fd5'] = 'Всего размещенных заказов (валидных):';
$_MODULE['<{statssales}prestashop>statssales_667c4007914c8a5d3ffd9f704aea2c3b'] = 'Всего заканых товаров (валидных):';
$_MODULE['<{statssales}prestashop>statssales_ec3e48bb9aa902ba2ad608547fdcbfdc'] = 'Продаж:';
$_MODULE['<{statssales}prestashop>statssales_0718e1ec4754e199aa9cb4bc43a6ff53'] = 'Вы можете видеть распределение заказов ниже';
$_MODULE['<{statssales}prestashop>statssales_3991390ca135c8c780b4a7d9c7c7daf5'] = 'За этот период нет заказов';
$_MODULE['<{statssales}prestashop>statssales_6602bbeb2956c035fb4cb5e844a4861b'] = 'Руководство';
$_MODULE['<{statssales}prestashop>statssales_cc23e852e21d85a834defe24867e3d05'] = 'Различные статусы заказов';
$_MODULE['<{statssales}prestashop>statssales_58d14111d598ec5174fe85dfebe71c56'] = 'Количество заказов и заказаных товаров';
$_MODULE['<{statssales}prestashop>statssales_7442e29d7d53e549b78d93c46b8cdcfc'] = 'Заказов';
$_MODULE['<{statssales}prestashop>statssales_2e80bb3c2f0ddd12eeb8204ce2906bd0'] = 'Заказов (выполнено)';
$_MODULE['<{statssales}prestashop>statssales_37510cd177c278556f1731d0c84fac59'] = 'Товаров (отгружено)';
$_MODULE['<{statssales}prestashop>statssales_14f1c54626d722168ee62dff05ed811e'] = 'Продаж';
$_MODULE['<{statssales}prestashop>statssales_33eee7690a8cd62490ed9eeadd47d163'] = 'Процентное соотношение заказов по статусу';
