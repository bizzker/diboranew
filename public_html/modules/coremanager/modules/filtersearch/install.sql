/* ################################## */
/* 			INSTALL NEW Tables		  */
/* ################################## */

CREATE TABLE IF NOT EXISTS `PREFIX_filter_group` (
  `id_filter_group` int(10) unsigned NOT NULL,
  `object` varchar(128) NOT NULL,  
  `global` tinyint(1) unsigned NOT NULL,
  `selected` tinyint(1) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id_filter_group`),
  KEY `filter_group_index` (`id_filter_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/* ################################## */
/* 		 INSTALL SAMPLE DATA			  */
/* ################################## */

INSERT INTO `PREFIX_filter_group` VALUES ('1', 'category', '0', '1', '1');
INSERT INTO `PREFIX_filter_group` VALUES ('2', 'attribute_group', '1', '1', '2');
INSERT INTO `PREFIX_filter_group` VALUES ('3', 'manufacturer', '1', '1', '3');
INSERT INTO `PREFIX_filter_group` VALUES ('4', 'feature', '1', '1', '4');