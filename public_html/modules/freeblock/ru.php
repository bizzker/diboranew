<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{freeblock}prestashop>freeblock_da0cf5d7515380af6416e5ae3c963c3c'] = 'Блок HTML или текста';
$_MODULE['<{freeblock}prestashop>freeblock_3b3de494900357cb3d88ce501cc5dc80'] = 'Добавить блок с любым текстом';
$_MODULE['<{freeblock}prestashop>freeblock_18524fb0d884d4f376fc4f0342953763'] = 'Вы уверены, что хотите удалить Ваши блоки с текстами?';
$_MODULE['<{freeblock}prestashop>freeblock_ae5b51e9d334144a370e041424ea7d49'] = 'Этот модуль позволяет Вам вставлять любой текст или HTML на сайт.';
$_MODULE['<{freeblock}prestashop>freeblock_39bedcaf3167b51f95b8a8338af2f81c'] = 'Для каждого языка Вы можете определить заголовок и содержание блока, и управлять расположением блока через стандартные Позиции модулей.';
$_MODULE['<{freeblock}prestashop>freeblock_32f5b1b5847ca03fa46d3840ed9cec60'] = 'Просто скопируйте Ваш текст или HTML код в зону ввода, чтобы увидеть это на Вашем сайте.';
$_MODULE['<{freeblock}prestashop>freeblock_8bb3c8770558329096b55c022c71e672'] = 'Однажды сохранив, предпросмотр Вашего контекта будет виден внизу этой страницы.';
$_MODULE['<{freeblock}prestashop>freeblock_7024e9c01ba8beec71d75549e45e898a'] = 'Подсказки:';
$_MODULE['<{freeblock}prestashop>freeblock_0f32c8fac3e6b826794719e66cfa22b3'] = '- Блок будет отображаться только для определенного языка.';
$_MODULE['<{freeblock}prestashop>freeblock_0ab57a2a5d945bfe660642e7057d9079'] = '- Удалить блок для языка можно просто его очистив.';
$_MODULE['<{freeblock}prestashop>freeblock_4b9932c7c5f5e8c938b24b2b24efd3b2'] = 'Свободный блок был успешно добавлен';
$_MODULE['<{freeblock}prestashop>freeblock_0cf0eb6fc073e1e84aecd407c442229c'] = 'Ошибка во время создания блока';
$_MODULE['<{freeblock}prestashop>freeblock_86c4e7aa0021c94078482df115e9cc17'] = 'Управлять блоками';
$_MODULE['<{freeblock}prestashop>freeblock_51ec9bf4aaeab1b25bb57f9f8d4de557'] = 'Заголовок:';
$_MODULE['<{freeblock}prestashop>freeblock_f18074f8b138ed9e289a2210c2b86838'] = 'Содержание:';
$_MODULE['<{freeblock}prestashop>freeblock_0d6d1ae0077be7f9d0fac969c06282aa'] = 'Сохранить';
$_MODULE['<{freeblock}prestashop>freeblock_3aea427a69e8906b95790872a9ab07cd'] = 'Предпросмотр:';
