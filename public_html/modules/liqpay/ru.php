<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{liqpay}prestashop>liqpay_533dc5e4d3d16346aec8221b313c0a7c'] = 'Принимать платежи с LiqPay';
$_MODULE['<{liqpay}prestashop>liqpay_fa214007826415a21a8456e3e09f999d'] = 'Вы уверены, что хотите удалить свои данные?';
$_MODULE['<{liqpay}prestashop>liqpay_10dcd2db95dac14ac1c6bb821915c246'] = 'Ваш LiqPay аккунт должен быть настроен правильно (укажите пароль и уникальный ID торговца)';
$_MODULE['<{liqpay}prestashop>liqpay_49d3f46e1bb439e8cfe82067a7346289'] = 'Merchant ID обязателен';
$_MODULE['<{liqpay}prestashop>liqpay_c4d12d07b81b2d8ce4a0c4bbe1a8bb59'] = 'Merchant пароль обязателен.';
$_MODULE['<{liqpay}prestashop>liqpay_e0aa021e21dddbd6d8cecec71e9cf564'] = 'Ок';
$_MODULE['<{liqpay}prestashop>liqpay_c888438d14855d7d96a2724ee9c306bd'] = 'Настройки были обновлены';
$_MODULE['<{liqpay}prestashop>liqpay_c4c0da756139dd27247bc33f44376cff'] = 'Этот модуль позволяет Вам принимать платежи с помощью платежной системы LiqPay';
$_MODULE['<{liqpay}prestashop>liqpay_d5937eabe75e1961eda5309200bfbe5e'] = 'Вам необходимо зарегистрироваться на сайте';
$_MODULE['<{liqpay}prestashop>liqpay_5dd532f0a63d89c5af0243b74732f63c'] = 'Контактная информация';
$_MODULE['<{liqpay}prestashop>liqpay_4d0a3b2765d9889e57d02a0bc51fc13a'] = 'Пожалуйста, укажите пароль и уникальный ID торговца, зарегистрированного в системе LiqPay';
$_MODULE['<{liqpay}prestashop>liqpay_229a7ec501323b94db7ff3157a7623c9'] = 'Merchant ID';
$_MODULE['<{liqpay}prestashop>liqpay_315f3c78ea54c8dadb082acc328dd5bd'] = 'Merchant пароль';
$_MODULE['<{liqpay}prestashop>liqpay_b17f3f4dcf653a5776792498a9b44d6a'] = 'Обновить настройки';
$_MODULE['<{liqpay}prestashop>liqpay_f5bb84824c03075e1eef6c1993697cbb'] = 'Оплатить с помощью платежной карты Visa, MasterCard';
