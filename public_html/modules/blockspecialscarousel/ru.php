<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_17c45c52c8aaec96c7eb02d5c8d3c4c4'] = 'Блок скидки с каруселью';
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_8878bd8e86a6f57aacd3f6962f07ad77'] = 'Добавляет блок с текущими скидками';
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_e2cb2a6247177597201f6cde43f20504'] = 'Время перехода обязательно.';
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_98f11ccb8697b44e890305d5cfaf3e2d'] = 'Врея задержки обязательно.';
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_e0aa021e21dddbd6d8cecec71e9cf564'] = 'Сохранить';
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_c888438d14855d7d96a2724ee9c306bd'] = 'Данные сохранены';
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_3225a10b07f1580f10dee4abc3779e6c'] = 'Параметры';
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_6e29fd0a007dbc596bb95b6570e8ff8a'] = 'Уажите время перехода и задержки в мс';
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_a61267725b9af3cb63bf08e92aedd4f0'] = 'Время задержки';
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_5c70d347793f358e80226040ecafc153'] = 'Время перехода';
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_ca7fa26c3e87ac3a982c48935c5c14c5'] = 'Скрыть, если скидок нет';
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Разрешить';
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_b9f5c797ebbf55adccdd8539a65a0241'] = 'Запретить';
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_192a5f89a85cbf1c0c3586273d70ca47'] = 'Этот блок не будет показан, если скидок нет';
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_b17f3f4dcf653a5776792498a9b44d6a'] = 'Обновить настройки';
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_d1aa22a3126f04664e0fe3f598994014'] = 'Скидки для Вас';
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_b4f95c1ea534936cc60c6368c225f480'] = 'Показать все скидки';
$_MODULE['<{blockspecialscarousel}prestashop>blockspecialscarousel_fd21fcc9fc4c1d5202d6fc11597b3fca'] = 'Пока скидок нет';
