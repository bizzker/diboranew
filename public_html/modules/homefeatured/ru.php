<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{homefeatured}prestashop>homefeatured_cd2c2dc24eaf11ba856da4a2e2a248be'] = 'Популярные товары на главной';
$_MODULE['<{homefeatured}prestashop>homefeatured_b494beb5de5dd7b4fad67262e0de6321'] = 'Отображает популярные товары на главной странице';
$_MODULE['<{homefeatured}prestashop>homefeatured_ced2b9f1aad1f8a5b351a3120e4b1212'] = 'Неверный номер товара';
$_MODULE['<{homefeatured}prestashop>homefeatured_c888438d14855d7d96a2724ee9c306bd'] = 'Настройки обновлены';
$_MODULE['<{homefeatured}prestashop>homefeatured_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{homefeatured}prestashop>homefeatured_6df1f9b5662398a551a2c9185b26638a'] = 'Для того, чтобы добавить популярные товары на главную страницу, просто добавьте их в домашнюю категорию.';
$_MODULE['<{homefeatured}prestashop>homefeatured_20c47700083df21a87e47d9299ef4dc4'] = 'Количество отображаемых товаров';
$_MODULE['<{homefeatured}prestashop>homefeatured_ada7594fb1614c6b6d271699c8511d5e'] = 'Количество товаров отображаемых на главной странице (по умолчанию: 10)';
$_MODULE['<{homefeatured}prestashop>homefeatured_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{homefeatured}prestashop>homefeatured_ca7d973c26c57b69e0857e7a0332d545'] = 'Новые поступления растений';
$_MODULE['<{homefeatured}prestashop>homefeatured_d3da97e2d9aee5c8fbe03156ad051c99'] = 'Подробнее';
$_MODULE['<{homefeatured}prestashop>homefeatured_4351cfebe4b61d8aa5efa1d020710005'] = 'Просмотр';
$_MODULE['<{homefeatured}prestashop>homefeatured_2d0f6b8300be19cf35e89e66f0677f95'] = 'Добавить в корзину';
$_MODULE['<{homefeatured}prestashop>homefeatured_e0e572ae0d8489f8bf969e93d469e89c'] = 'Популярных товаров нет';