<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockweather}prestashop>blockweather_b2373cf6068ba6479d64ed32723c0cc6'] = 'Bloc météo';
$_MODULE['<{blockweather}prestashop>blockweather_2ca53fa82e5d2c77059cc73933e85170'] = 'Ajoute un bloc affichant les prévisions météo provenant de l\'API Google Weather.';
$_MODULE['<{blockweather}prestashop>blockweather_c1cb8d7ee2c546cf9016028c52809236'] = 'Nom de ville invalide.';
$_MODULE['<{blockweather}prestashop>blockweather_70afb9f36ab64ba24cc9c624c183582e'] = 'Cette ville n\'est pas présente dans l\'API Google Weather.';
$_MODULE['<{blockweather}prestashop>blockweather_c888438d14855d7d96a2724ee9c306bd'] = 'Configuration mis à jour';
$_MODULE['<{blockweather}prestashop>blockweather_f4f70727dc34561dfde1a3c529b6205c'] = 'Configuration';
$_MODULE['<{blockweather}prestashop>blockweather_57d056ed0984166336b7879c2af3657f'] = 'Ville';
$_MODULE['<{blockweather}prestashop>blockweather_b5928be1c444086357f1ff2b7f781bab'] = 'Précisez une ville';
$_MODULE['<{blockweather}prestashop>blockweather_c9cc8cce247e49bae79f15173ce97354'] = 'Sauvegarder';
$_MODULE['<{blockweather}prestashop>blockweather_948a2e3548aaf7f9941a3192fa607d51'] = 'Crédits';
$_MODULE['<{blockweather}prestashop>blockweather_0f0e2725fe3ef957feb0c9888856a3d9'] = 'Module crée par';
$_MODULE['<{blockweather}prestashop>blockweather_2c7482028a3ebaffed2bb02dd70660be'] = 'Veuillez m\'envoyer tous retours, bugs ou autres via le formulaire de mon site marchand.';
$_MODULE['<{blockweather}prestashop>blockweather_4ea12d2fbc4fcdd0797323941f612fdb'] = 'Si vous traduisez ce module dans votre langue, veuillez m\'envoyer votre fichier de langue par mail à';
$_MODULE['<{blockweather}prestashop>blockweather_862509a1df105d07cea720c09eab46e9'] = 'Merci beaucoup par avance !';
$_MODULE['<{blockweather}prestashop>blockweather_3c27fd116cc7e6c594c0cb3f4d2ff09f'] = 'Liste des traducteurs !';
$_MODULE['<{blockweather}prestashop>blockweather_ec15d66682fa74fc439d42041b72a4cd'] = 'Prévisions météo';
$_MODULE['<{blockweather}prestashop>blockweather_be9469719aa6efa2fc3bd5768bc406d7'] = 'Lieu :';
$_MODULE['<{blockweather}prestashop>blockweather_0cf80ce1aa66e69b6d2708d0af827247'] = 'Température :';
$_MODULE['<{blockweather}prestashop>blockweather_b013f617a79679efc4786f7f733d4f8e'] = 'Mini :';
$_MODULE['<{blockweather}prestashop>blockweather_7f0e3aa79e23ddbf2291d54fe6f373e4'] = 'Maxi :';
