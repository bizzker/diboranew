<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockweather}prestashop>blockweather_b2373cf6068ba6479d64ed32723c0cc6'] = ' Блок погоды';
$_MODULE['<{blockweather}prestashop>blockweather_2ca53fa82e5d2c77059cc73933e85170'] = 'Добавляет блок погоды от Google Weather API.';
$_MODULE['<{blockweather}prestashop>blockweather_c1cb8d7ee2c546cf9016028c52809236'] = 'Такого города нет.';
$_MODULE['<{blockweather}prestashop>blockweather_70afb9f36ab64ba24cc9c624c183582e'] = 'Этот город не поддерживается  Google Weather API.';
$_MODULE['<{blockweather}prestashop>blockweather_c888438d14855d7d96a2724ee9c306bd'] = 'Настройки обновлены';
$_MODULE['<{blockweather}prestashop>blockweather_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{blockweather}prestashop>blockweather_57d056ed0984166336b7879c2af3657f'] = 'Город';
$_MODULE['<{blockweather}prestashop>blockweather_b5928be1c444086357f1ff2b7f781bab'] = 'Заполните название города';
$_MODULE['<{blockweather}prestashop>blockweather_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{blockweather}prestashop>blockweather_948a2e3548aaf7f9941a3192fa607d51'] = 'Кредиты';
$_MODULE['<{blockweather}prestashop>blockweather_0f0e2725fe3ef957feb0c9888856a3d9'] = 'Модуль создан';
$_MODULE['<{blockweather}prestashop>blockweather_2c7482028a3ebaffed2bb02dd70660be'] = 'Присылайте баги, фичи.';
$_MODULE['<{blockweather}prestashop>blockweather_4ea12d2fbc4fcdd0797323941f612fdb'] = 'Если Вы переведете модуль на свой язык, пришлите его мне';
$_MODULE['<{blockweather}prestashop>blockweather_862509a1df105d07cea720c09eab46e9'] = 'Спасибо!';
$_MODULE['<{blockweather}prestashop>blockweather_3c27fd116cc7e6c594c0cb3f4d2ff09f'] = 'Список тягачей';
$_MODULE['<{blockweather}prestashop>blockweather_ec15d66682fa74fc439d42041b72a4cd'] = 'Погода в Киеве';
$_MODULE['<{blockweather}prestashop>blockweather_be9469719aa6efa2fc3bd5768bc406d7'] = 'Место';
$_MODULE['<{blockweather}prestashop>blockweather_0cf80ce1aa66e69b6d2708d0af827247'] = 'Температура';
$_MODULE['<{blockweather}prestashop>blockweather_b013f617a79679efc4786f7f733d4f8e'] = 'От:';
$_MODULE['<{blockweather}prestashop>blockweather_7f0e3aa79e23ddbf2291d54fe6f373e4'] = 'До:';
