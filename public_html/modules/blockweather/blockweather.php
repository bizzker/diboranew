<?php
/**
  * BlockWeather class, blockweather.php
  * Module Block Weather
  * @category modules
  *
  * @author Joel GAUJARD <contact@joelgaujard.info>
  * @copyright JoelGaujard.info
  * @license http://prestashop.joelgaujard.info/content/3-terms-and-conditions-of-use
  *
  */

require_once(_PS_MODULE_DIR_.'blockweather/lib/GoogleWeather.class.php');

class BlockWeather extends Module
{
	function __construct()
	{
		$this->name = 'blockweather';
		$this->tab = 'JoelGaujard.info'; // Blocks

		parent::__construct();

		$this->displayName = $this->l('Weather forecast block');
		$this->description = $this->l('Adds a block displaying weather forecast from Google Weather API.');
		$this->version = '1.2.0'; // Match with PS 1.2.*

		$this->error = false;
		$this->valid = false;
	}

	function install()
	{
		if (!parent::install()
			OR !Configuration::updateValue('BLOCK_WEATHER_CITY', 'New York')
			OR !$this->registerHook('rightColumn')
		)
			return false;
		return true;
	}

	public function getContent()
	{
		$output = '<h2>'.$this->displayName.'</h2>';
		if (Tools::isSubmit('submitModule'))
		{
			$city = strval(Tools::getValue('city'));
			if ($city AND !Validate::isCityName($city))
				$errors[] = $this->l('Invalid city name.');
			else
			{
				$gweather = new GoogleWeatherAPI($city);
				if(!$gweather->isFound())
					$errors[] = $this->l('This city is not supported by Google Weather API.');
			}
			if (isset($errors) AND sizeof($errors))
				$output .= $this->displayError(implode('<br />', $errors));
			else
			{
				Configuration::updateValue('BLOCK_WEATHER_CITY', $city);
				$output .= $this->displayConfirmation($this->l('Settings updated'));
			}
		}
		$output .= $this->displayForm();
		$output .= '<br class="clear" /><br/>'.$this->_displayCredits();
		return $output;
	}

	public function displayForm()
	{
		$output = '
		<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
			<fieldset><legend><img src="'.$this->_path.'weather.gif" alt="" title="" />'.$this->l('Settings').'</legend>
				<label for="city">'.$this->l('City').'</label>
				<div class="margin-form">
					<input type="text" name="city" id="city" value="'.Tools::getValue('city', Configuration::get('BLOCK_WEATHER_CITY')).'" />
					<p class="clear">'.$this->l('Fill a city name').'</p>
				</div>
				<center><input type="submit" name="submitModule" value="'.$this->l('Save').'" class="button" /></center>
			</fieldset>
		</form>';
		return $output;
	}

	function hookLeftColumn($params)
	{
		global $smarty, $cookie;
		$id_lang = intval($cookie->id_lang);
		$iso_lang = Language::getIsoById($id_lang) !== false ? Language::getIsoById($id_lang) : 'en';

		// Conf
		$city = strval(Configuration::get('BLOCK_WEATHER_CITY'));

		// Getting data

		$gweather = new GoogleWeatherAPI($city, $iso_lang);
		if(!$gweather->isFound())
			return false;

		// Display smarty
		$smarty->assign(array(
			'city' => $gweather->getCity(),
			'current_conditions' => $gweather->getCurrent(),
			'forecast_conditions' => $gweather->getForecast()
		));
		return $this->display(__FILE__, $this->name.'.tpl');
	}
	function hookRightColumn($params)
	{
		return $this->hookLeftColumn($params);
	}
	function hookTop($params)
	{
		return $this->hookLeftColumn($params);
	}
	function hookFooter($params)
	{
		return $this->hookLeftColumn($params);
	}
	function hookHome($params)
	{
		return $this->hookLeftColumn($params);
	}

	private function _displayCredits()
	{
		$output = '
		<form action="#" method="post">
			<fieldset class="width3">
				<legend><img src="'.$this->_path.'logo.gif" alt="jg" />'.$this->l('Credits').'</legend>
				<p>
					'.$this->l('Module created by').'
					<a href="http://www.joelgaujard.info"><b>Jo&euml;l Gaujard</b> - http://www.joelgaujard.info</a>.
				</p>
				<p>
					<a href="http://prestashop.joelgaujard.info/contact-form.php" style="text-decoration: underline;">
						'.$this->l('Please send me feedback, bugs or other stuff via my shop contact form.').'
					</a>
				</p>
				'.$this->_displayTranslators().'
				<p>
					'.$this->l('If you translate this module in your language, please send me your language file by email at').'
					<a href="mailto:translations@joelgaujard.info">
						translations@joelgaujard.info
					</a>.
					'.$this->l('Many thanks in anticipation!').'
				</p>
			</fieldset>
		</form>';
		return $output;
	}

	private function _displayTranslators()
	{
		$translators = array(
			"fr" => array(
				"fullname" => "Joel Gaujard",
				"url" => "http://blog.joelgaujard.info"
			)
		);
		if (empty($translators) OR count($translators) < 1)
			return '';
		$output = '
		<p>
			<h3>'.$this->l('Traductors list!').'</h3>
			<ul>';
		foreach ($translators AS $lang_iso => $translator)
		{
			$output .= '<li>
				<img src="http://www.google.fr/images/flags/'.$lang_iso.'_flag.png" alt="'.$lang_iso.'" width="16" height="11" style="vertical-align: middle;" />
				<a target="_blank" href="'.$translator["url"].'">'.$translator["fullname"].' - '.$translator["url"].'</a>';
			$output .= '</li>';
		}
		$output .= '</ul>
		</p>';
		return $output;
	}

}
