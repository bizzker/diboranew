<!-- Block Weather module by Joel Gaujard -->
<div class="block weather_block">
	<h4>{l s='Wheater forecast' mod='blockweather'}</h4>
	<div class="block_content">
		<p>
			<h5>{l s='Location:' mod='blockweather'} {$city}</h5>
			<br/><img src="{$current_conditions.icon}" alt="{$current_conditions.condition}" align="left" />
			<span class="bold">{$current_conditions.condition}</span>
		</p>
		<p class="clear">
			{l s='Temperature:' mod='blockweather'} {$current_conditions.temp_c}&deg;C
			<br/>{l s='Temperature:' mod='blockweather'} {$current_conditions.temp_f}&deg;F
			<br/>{$current_conditions.humidity}
			<br/>{$current_conditions.wind_condition}
		</p>
		<hr/>
		<ul>
			{foreach from=$forecast_conditions item=forecast_condition name=loop}
			<li>
				{$forecast_condition.day_of_week}
				<br/><img src="{$forecast_condition.icon}" alt="{$forecast_condition.condition}" align="left" />
				<span class="bold">{$forecast_condition.condition}</span>
				<br/>{l s='Low:' mod='blockweather'} {$forecast_condition.low}&deg;
				<br/>{l s='High:' mod='blockweather'} {$forecast_condition.high}&deg;
			</li>
			{/foreach}
		</ul>
	</div>
</div>
<!-- /Block Weather module by Joel Gaujard -->
