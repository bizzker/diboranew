<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsprofitmargin}prestashop>statsprofitmargin_4f29d8c727dcf2022ac241cb96c31083'] = 'Отримано порожній рядок';
$_MODULE['<{statsprofitmargin}prestashop>statsprofitmargin_eab2b4237a7cd89c309119e35f62d168'] = 'Показано';
$_MODULE['<{statsprofitmargin}prestashop>statsprofitmargin_8bf8854bebe108183caeb845c7676ae4'] = 'з';
$_MODULE['<{statsprofitmargin}prestashop>statsprofitmargin_b718adec73e04ce3ec720dd11a06a308'] = 'ID';
$_MODULE['<{statsprofitmargin}prestashop>statsprofitmargin_96b0141273eabab320119c467cdcaf17'] = 'Всього';
$_MODULE['<{statsprofitmargin}prestashop>statsprofitmargin_182875b6c36a761f1e459624c1d93638'] = 'Прибуток';
$_MODULE['<{statsprofitmargin}prestashop>statsprofitmargin_54358a914f51e1af19df8520159fe607'] = 'Доход';
$_MODULE['<{statsprofitmargin}prestashop>statsprofitmargin_ea9cf7e47ff33b2be14e6dd07cbcefc6'] = 'Доставка';
$_MODULE['<{statsprofitmargin}prestashop>statsprofitmargin_9d5bf15117441a1b52eb1f0808e4aad3'] = 'Знижки';
$_MODULE['<{statsprofitmargin}prestashop>statsprofitmargin_31c3377ae9fd0ca8f3f05d57ee343904'] = 'Прибуток';
$_MODULE['<{statsprofitmargin}prestashop>statsprofitmargin_f9bc550983561857c2eb011fe161d4ee'] = 'Перелік прибутків із замовлень';
$_MODULE['<{statsprofitmargin}prestashop>statsprofitmargin_38475393398dc49c9242ff83872876df'] = 'Всього прибуток:';
$_MODULE['<{statsprofitmargin}prestashop>statsprofitmargin_c1c1cc7cdf912769d6b4c3582076a738'] = 'Всього знижок:';
$_MODULE['<{statsprofitmargin}prestashop>statsprofitmargin_543ae6adeb524f98cc7d3c816a1ec1e3'] = 'На складі:';
$_MODULE['<{statsprofitmargin}prestashop>statsprofitmargin_54df9536f2bc89e18bec27b0d34ab42e'] = 'Завантажити CSV';
$_MODULE['<{statsprofitmargin}prestashop>statsprofitmargin_0d8d58008ef5dd7afce337373ef73993'] = 'Дата:';
