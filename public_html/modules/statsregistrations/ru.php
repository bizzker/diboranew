<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsregistrations}prestashop>statsregistrations_8b15fc6468c919d299f9a601b61b95fc'] = 'Аккаунты клиентов';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_54c04cc0d869c12ea9f5426466d40fea'] = 'Отображает регистрационную панель для покупателя';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_75abad0b891bc086fd4837e81a8c488a'] = 'Посетители, прекратившие регитрацию на первом шаге:';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_0d298316c16a0bbdb74d50ead21590b4'] = 'Посетители, сделавшие заказ сразу после регистрации:';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_a751e9cc4ed4c7585ecc0d97781cb48a'] = 'Всего зарегестрированных аккаунтов покупателей:';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_6602bbeb2956c035fb4cb5e844a4861b'] = 'Руководство';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_fba0e64541196123bbf8e3737bf9287b'] = 'Количество созданных аккаунтов';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_15d820295ae2d9d7e6d53e1ed9714273'] = 'Общее количество созданных аккаунтов это не очень важная информация. Хотя временами интересно узнать эту информацию. Это предоставит информацию о том, все ли идет как надо.';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_2d8c349a97131b417bf7da166afb17bd'] = 'Если вы запустите магазин без изменений, количество регистраций покупателей будет стабильным или понемногу падать.';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_bf88699f842b6e63e0568ed0e71c68b7'] = 'Значительное увеличение или уменьшение количества регистраций показывает, что в магазине произошли изменения; однако, вам следует все проверить, если изменения прошли в сторону уменьшения, или стоит на месте, или двигаться в сторону увеличения.';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_7f93ad00e5438305b72efd723da48cdb'] = 'Сдесь сводка о том, что может повлиять на создание аккаунтов покупателей:';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_c22f6af88c5ec8dcdba70e4fa394b08e'] = 'Рекламная кампания может содействовать увеличению количества посетителей. Увеличение количества аккаунтов покупателей также зависит от их \\\"качества\\\": хорошо проработанная реклама с четкими целями может быть более эффективна чем реклама общего назначения.';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_d140e86c9fd44c00ed1df75b92ce8115'] = 'Содержание, продажи или контексты привлекают больше внимания, делают магазин живим и увеличивают трафик. Таким способом вы можете способствовать импульсивным покупателям сделать выбор.';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_97d6defbec1f8cd01e8219da3e917a0b'] = 'Дизайн и дружественность сейчас очень важны: неудобная и некрасивая тема может оттолкнуть посетителей. Вы должны поддерживать баланс между инновационным дизайном и удобством навигации.  ';
