<?php
/**
 * Cron module
 * Cron is a time-based job scheduler in Unix-like computer operating systems.
 * This module automaticaly executes jobs like Cron
 *
 * Add a cron job
 * Module::getInstanceByName('cron')->addCron($this->id, 'myMethod', '5 * * * *');
 *
 * last parameter details :
 * .---------------- minute (0 - 59) 
 * |  .------------- hour (0 - 23)
 * |  |  .---------- day of month (1 - 31)
 * |  |  |  .------- month (1 - 12) 
 * |  |  |  |  .---- day of week (0 - 6) (Sunday=0 )
 * |  |  |  |  |
 * *  *  *  *  *
 *
 * remarks :
 * It accepts the standard crontab format except steps ('/') :  0-50/5 * * * * isn't valid
 * 
 * exemples :
 * - 1 0 * * * : 00:01 of every day of the month, of every day of the week
 * - 15 3 * * 1-5 : every weekday morning at 3:15 am
 * - 0 0 1,15-17 * * : the first, fifteenth, sixteenth and seventeenth of each month at 00:00
 * - 0 0 * * 1 : every Monday at 00:00
 * 
 * Delete a job
 * Module::getInstanceByName('cron')->deleteCron($this->id, 'myMethod');
 * 
 * @category Prestashop
 * @category Module
 * @author Samdha <contact@samdha.net>
 * @copyright Samdha
 * @license http://www.opensource.org/licenses/osl-3.0.php Open-source licence 3.0
 * @author logo Alessandro Rei http://www.kde-look.org/content/show.php/Dark-Glass+reviewed?content=67902
 * @license logo http://www.gnu.org/copyleft/gpl.html GPLv3
 * @version 1.0
**/
class cron extends Module
{
	const INSTALL_SQL_FILE = 'install.sql';

	public function __construct()
	{
		$this->name = 'cron';
		$this->tab = 'Tools';
		$this->version = '1.0';

		parent::__construct();

		$this->displayName = $this->l('Crontab for Prestashop');
		$this->description = $this->l('Allows modules to schedule jobs to run automatically at a certain time or date.');
	}
	
	/**
	 * install the module
	 *
	 * create table in BDD
	 * hook the module to footer
	**/
	public function install()
	{
		require_once(dirname(__FILE__).'/CronParser.php');
		if (!file_exists(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE))
			return (false);
		else if (!$sql = file_get_contents(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE))
			return (false);
		$sql = str_replace('PREFIX_', _DB_PREFIX_, $sql);
		$sql = preg_split("/;\s*[\r\n]+/", $sql);
		foreach ($sql AS $query)
			if($query)
				if(!Db::getInstance()->Execute(trim($query)))
					return false;
				
	 	if (!parent::install()
			OR !$this->registerHook('footer')
			OR !Configuration::updateValue('cron_method', 'traffic')
			OR !Configuration::updateValue('cron_test', 0)
			)
	 		return false;
		return true;
	}
	
	public function uninstall()
	{
		return (Configuration::deleteByName('cron_lasttime') AND
				Configuration::deleteByName('cron_method') AND
				Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'cron') AND
				parent::uninstall());
	}

    public function getContent() {
        $output .= '<h2>'.$this->displayName.'</h2>';

        if (!empty($_POST))
			$output .= $this->_postProcess();
        $output .= $this->_displayForm();

		return $output;
    }

	private function _displayConf()
	{
		return '
		<div class="conf confirm">
			<img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />
			'.$this->l('Settings updated').'
		</div>';
	}

    private function _displayForm() {
		global $cookie;
		
		$output = '';
		$cron_test = Configuration::get('cron_test');
		if ($cron_test) {
			if ($cron_lasttest = Configuration::get('cron_lasttest'))
				$output .= '
					<div class="conf confirm" style="width:898px">
						<img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />
						'.$this->l('Last test have been successfully executed on').' '.Tools::DisplayDate(date('Y-m-d H:i:s', $cron_lasttest), $cookie->id_lang, true).' 
					</div>';
			else
				$output .= '
					<div class="alert">
						'.$this->l('Test have not been executed yet.').' 
					</div>';
		}
		
		$cron_method = Configuration::get('cron_method');
		$output .= '
	        <form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
				<fieldset style="float: right; width: 255px">
					<legend>'.$this->l('About').'</legend>
					<p style="font-size: 1.5em; font-weight: bold; padding-bottom: 0"><img src="'.$this->_path.'logo.png" alt="'.$this->displayName.'" style="float: left; padding-right: 1em"/>'.$this->displayName.'</p>
					<p style="clear: both">
					'.$this->description.'
					</p>
					<p>
					'.$this->l('Developped with love by').' <a style="color: #7ba45b; text-decoration: underline;" href="http://www.samdha.net">Samdha</a>'.$this->l(', which helps you develop your e-commerce site.').'
					</p>
					<p>
					<a href="http://www.samdha.net/contactez-nous"><img src="../img/admin/email.gif" alt="" /> '.$this->l('Contact').'</a>
					</p>
				</fieldset>
				
				<fieldset class="width3">
					<legend>'.$this->l('Parameters').'</legend>
					
					<p>
					'.$this->l('Thanks for installing the cron module on your website.').'
					</p>
					<p>
					'.$this->l('Please choose the method used to determine when executing jobs.').'
					</p>
					
					<label for="cron_method">'.$this->l('Method').'</label>
					<div class="margin-form">
						<select name="cron_method" id="cron_method">
							<option value="traffic" '.($cron_method == 'traffic'?'selected="selected"':'').'>'.$this->l('Shop traffic').'</option>
							<option value="crontab" '.($cron_method == 'crontab'?'selected="selected"':'').'>'.$this->l('Server crontab').'</option>
							<option value="webcron" '.($cron_method == 'webcron'?'selected="selected"':'').'>'.$this->l('Webcron service').'</option>
						</select>
					</div>
					
					<hr/>
					<p>
					'.$this->l('"Shop traffic" method doesn\'t need configuration but is not sure. It depends of your website frequentation so when it isn\'t visited, jobs are not executed.').'
					</p>
					<hr/>
					<p>
					'.$this->l('"Server crontab" is the best method but only if your server uses Linux and you have access to crontab. In that case add the line below to your crontab file.').'
					</p>
					<code>* * * * * php -f '.dirname(__FILE__).DIRECTORY_SEPARATOR.'cron_crontab.php</code>
					<hr/>
					<p>
					'.$this->l('"Webcron service" is a good alternative to crontab but is often not free. Register to a service like').' <a href="http://www.webcron.org">webcron.org</a> 
					'.$this->l('and configure it to visit the URL below every minutes or the nearest.').'
					</p>
					<code>http://'.$this->getHttpHost(false, true).$this->_path.'cron_webcron.php</code>

					<hr/>
					<p>
					'.$this->l('To check whether the choosen method works, you can enable the test job. It should be executed every minutes and show it at the top of this form. When everything is ok you can disable it.').'
					</p>
					<label for="cron_test">'.$this->l('Test job').'</label>
					<div class="margin-form">
						<select name="cron_test" id="cron_test">
							<option value="1" '.($cron_test?'selected="selected"':'').'>'.$this->l('Enable').'</option>
							<option value="0" '.(!$cron_test?'selected="selected"':'').'>'.$this->l('Disable').'</option>
						</select>
					</div>
				</fieldset>
				<p><input type="submit" class="button" name="submitAddThis" value="'.$this->l('Save').'" /></p>
			</form>
		';
		return $output;
    }
	
	private function _postProcess() {
		if (Tools::getValue('cron_method'))
			Configuration::updateValue('cron_method', Tools::getValue('cron_method'));
		
		if (Configuration::get('cron_test') != Tools::getValue('cron_test')) {
			if (Tools::getValue('cron_test'))
				$this->addTest();
			else
				$this->deleteTest();
			Configuration::updateValue('cron_test', Tools::getValue('cron_test'));
		}
		
		$output .= $this->_displayConf();
		return $output;
	}

	/**
	 * add a false picture to run task in background
	**/
	public function hookFooter($params)
	{
		if (Configuration::get('cron_method') == 'traffic' &&
			(!Configuration::get('cron_lasttime') ||
			 (Configuration::get('cron_lasttime') + 60 <= time())
			))
			return '<img src="'.$this->_path.'cron_traffic.php?time='.time().'" alt="cron module by samdha.net" width="0" height="0" style="border:none;margin:0; padding:0"/>';
	}
	
	/**
	 * add a cron job
	 * 
	 * usage Module::getInstanceByName('cron')->addCron($this->id, 'myMethod', '5 * * * *');
	 *
	 * $mhdmd details :
	 * .---------------- minute (0 - 59) 
	 * |  .------------- hour (0 - 23)
	 * |  |  .---------- day of month (1 - 31)
	 * |  |  |  .------- month (1 - 12) 
	 * |  |  |  |  .---- day of week (0 - 6) (Sunday=0 )
	 * |  |  |  |  |
	 * *  *  *  *  *
	 * 
	 * @param int $id_module Module ID
	 * @param string $method method of the module to call
	 * @param string $mhdmd when call this cron
	 * @return boolean
	**/
	public function addCron($id_module, $method, $mhdmd = '0 * * * *') {
		if (!$this->active)
			return false;
		require_once(dirname(__FILE__).'/CronParser.php');
		if (!$module = Module::getInstanceById($id_module))
			return false;
		$classMethods = array_map(strtolower, get_class_methods($module));
		if (!$classMethods || !in_array(strtolower($method), $classMethods))
			return false;
		$cronParser = new CronParser();
		if (!$cronParser->calcLastRan($mhdmd))
			return false;
		
		$values = array(
						'id_module' => intval($id_module),
						'method' => pSQL($method),
						'mhdmd' => pSQL($mhdmd),
						'last_execution' => 0
					   );
		return Db::getInstance()->autoExecute(_DB_PREFIX_.'cron', $values, 'INSERT');
	}
	
	/**
	 * delete a cron job
	 *
	 * @param int $id_module Module ID
	 * @param string $method method of the module to call
	 * @return boolean
	**/
	public function deleteCron($id_module, $method) {
		if (!$this->active)
			return false;
		return Db::getInstance()->delete(_DB_PREFIX_.'cron','`id_module` = '.intval($id_module).' AND `method` = \''.pSQL($method).'\'');		
	}
	
	/**
	 * execute cron jobs
	 * invalide job will be deleted
	 *
	 * @return void
	**/
	public function runJobs() {
		if ($this->active &&
			(Configuration::get('cron_lasttime') + 60 <= time())) {
			Configuration::updateValue('cron_lasttime', time());
			require_once(dirname(__FILE__).'/CronParser.php');
			
			$cronParser = new CronParser();
			// get the jobs
			$sql = 'SELECT * FROM `'._DB_PREFIX_.'cron`';
			$crons = Db::getInstance()->executeS($sql);
			foreach ($crons as $cron) {
				// When the job should have been executed for the last time ?
				// if it's in the past execute it
				$cronParser->calcLastRan($cron['mhdmd']);
				if ($cronParser->getLastRanUnix() > $cron['last_execution']) {
					// if module doesn't exists delete job
					if (!$module = Module::getInstanceById($cron['id_module'])) {
						$this->deleteCron($cron['id_module'], $cron['method']);
					}
					else {
						$classMethods = array_map(strtolower, get_class_methods($module));
						// if method doesn't exists delete job
						if (!$classMethods || !in_array(strtolower($cron['method']), $classMethods)) {
							$this->deleteCron($cron['id_module'], $cron['method']);
						}
						else {
							$values = array(
								'last_execution' => time()
							);
							Db::getInstance()->autoExecute(_DB_PREFIX_.'cron', $values, 'UPDATE');
							// run job TODO: make it asynchronous
							call_user_method($cron['method'], $module);
						}
					}
				}
			}
		}
	}
	
	public function getHttpHost($http = false, $entities = false)
	{
		$host = (isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST']);
		if ($entities)
			$host = htmlspecialchars($host, ENT_COMPAT, 'UTF-8');
		if ($http)
			$host = (Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://').$host;
		return $host;
	}	
	
	/**
	 * tests method
	 * to show how to add/delete jobs
	**/
	public function addTest() {
		Configuration::deleteByName('cron_lasttest');
		return Module::getInstanceByName('cron')->addCron($this->id, 'test', '* * * * *');
	}
	public function deleteTest() {
		Configuration::deleteByName('cron_lasttest');
		return Module::getInstanceByName('cron')->deleteCron($this->id, 'test');
	}
	public function test() {
		// store the last time the test was executed
		return Configuration::updateValue('cron_lasttest', time());
	}
}

?>
