	$(document).ready(function(){
		$('#gallery').galleryView({
			panel_width: 526,
			panel_height: 198,
			frame_width: 100,
			frame_height: 100,
			overlay_height: 30,
      		transition_speed: 1000,
     		easing: 'easeInOutQuad'
		});
	});
