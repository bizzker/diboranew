<!-- MODULE jGalleryview -->
<script type="text/javascript" src="{$module_dir}assets/jquery.galleryview-2.0-pack.js"></script>
<script type="text/javascript" src="{$module_dir}assets/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="{$module_dir}assets/jquery.timers-1.1.2.js"></script>
<!--
jGalleryview core stylesheet
-->
<link rel="stylesheet" type="text/css" href="{$module_dir}assets/galleryview.css" />
<!--
jGalleryview template
-->
<script type="text/javascript" src="{$module_dir}assets/galleryview_start.js"></script>
<div id="galleryviewHolder" align="center" style="padding:0 0 10px 0;overflow: hidden; clear:both">
	<div id="wrap">
		<ul id="gallery">
			<li><span class="panel-overlay">На протяжение 10 лет работы мы собрали коллекцию из <b>более чем</b> 300 видов многолетних и однолетних растений.</span>
			<img src="{$module_dir}slides/02.jpg"> 
			<li><span class="panel-overlay">Мы выращиваем многолетние и однолетние растения в открытом грунте <b>без использования</b> стимуляторов роста.</span>
			<img src="{$module_dir}slides/01.jpg"> 
			<li><span class="panel-overlay">Все растения, выращенные нами, приспособлены к условиям <b>местного</b> климата и прошли <b>тщательный</b> отбор.</span>
			<img src="{$module_dir}slides/03.jpg"> 
			<li><span class="panel-overlay">Кроме этого, мы занимаемся <b>адаптацией и выращиванием</b> импортного посадочного материала.</span>
			<img src="{$module_dir}slides/04.jpg"> 
			<li><span class="panel-overlay">Мы проводим консультации по подбору цветов и растений, <b>максимально удовлетворяющие</b> всем требованиям.</span>
			<img src="{$module_dir}slides/05.jpg"> 
			<li><span class="panel-overlay">Мы консультируем по вопросам выращивания и посадки растений, исходя из <b>множества важных</b> факторов!</span>
			<img src="{$module_dir}slides/06.jpg"> 
			<li><span class="panel-overlay">Мы поможем Вам решить все вопросы по <b>уходу за цветами и растениями</b>.</span>
			<img src="{$module_dir}slides/07.jpg"> 
			<li><span class="panel-overlay">И, конечно же, мы занимаемся <b>доставкой цветов и растений</b> как по Киеву и области, так и по всей Украине.</span>
			<img src="{$module_dir}slides/08.jpg"> 
		</ul>
	</div>
</div>
<!-- /MODULE jGalleryview -->
