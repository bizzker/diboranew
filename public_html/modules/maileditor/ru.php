<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{maileditor}prestashop>maileditor_9e78fe4c09db858aa64bc98da00096c8'] = 'Редактор шаблонов писем';
$_MODULE['<{maileditor}prestashop>maileditor_bb3b4efc6d6370b128b7c9a4ba98133e'] = 'Этот редактор поможет Вам отредактировать все шаблоны писем, которые отсылаются сайтом. Вы можете использовать визуальный редактор для этого.';
$_MODULE['<{maileditor}prestashop>maileditor_9a1e2e9a3ab1c36a03f889c1d75a19af'] = 'Существующие шаблоны писем';
$_MODULE['<{maileditor}prestashop>maileditor_bf75f228011d1443c4ea7aca23c3cff2'] = 'Введение';
$_MODULE['<{maileditor}prestashop>maileditor_c1628177f8b2ee21a19600977849df3a'] = 'Обновить файл';
